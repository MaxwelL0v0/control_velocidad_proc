import processing.sound.*;
SoundFile file;
int freq = 10;
int loop_counter=0;
float seconds_counter;
int sqw=25;
int ind_h = 10;
IntList ind_array;
float v = 0;
float a = 0;
float f = 0;
float h = 0.1;
float s1X, s1Y, s2X, s2Y;
boolean overS1 = false;
boolean overS2 = false;
void setup () {
  size (700, 700);
  smooth();
  frameRate(freq);
  ind_array = new IntList();
  for(int i = 0; i<=20; i++){ind_array.append(i);};
  file = new SoundFile(this, "sample.mp3");
  s1X = width/4+3*sqw;
  s1Y = width/2;
  s2X = 3*width/4-4*sqw;
  s2Y = width/2;
}
void draw () {
  background(51,0,51);
  fill (225);
  stroke(75);
  rect(width/4+3*sqw, width/2, sqw, sqw);
  fill (75);
  stroke(225);
  rect(3*width/4-4*sqw, width/2, sqw, sqw);
  noFill();
  ellipse(3*width/4, width/4+2*sqw, 1.5*sqw, 1.5*sqw);
  ellipse(width/4, width/4+2*sqw, 1.5*sqw, 1.5*sqw);
  sliders();
  overS1 = overSensor(s1X, s1Y, sqw, sqw);
  overS2 = overSensor(s2X, s2Y, sqw, sqw);
  sens(overS1, overS2);
  text_ind();
}
boolean overSensor(float x, float y, int width, int height)  {
  if (mouseX >= x && mouseX <= x+width && 
      mouseY >= y && mouseY <= y+height) {
    return true;
  } else {
    return false;
  }
}
void sens(boolean s1, boolean s2){
  if (s1) {
      f = 0;
      a = a+h;
      v = v+a;
      loop_counter++;
      seconds_counter = loop_counter/freq;
      if (seconds_counter >= 10){
        if (!file.isPlaying()) {file.play();};
          fill(255,0,0);
          ellipse(width/4, width/4+2*sqw, 1.5*sqw, 1.5*sqw);
          noFill();
      };
      
  }
  else if (a>=h && !s2) {
      loop_counter=0;
      file.stop();
      a = a-h;
      v = v+a;
  }
  else if (v>0) {
    if (s2){
      fill(255,0,0);
      ellipse(3*width/4, width/4+2*sqw, 1.5*sqw, 1.5*sqw);
      a = 0;
      v = v-f;
      f = f+h;
    }
    else if (f>h) {
      f=f-h;
      v = v-f;
    };
  } 
  else{
      v = 0;
      a = 0;
      f = 0;
  }
}
void text_ind(){
  fill(102,0,0);
  stroke(225);
  // aceleracion
  rect(width/4-2*sqw, 3*width/4-sqw, 6*sqw, 3*sqw);
  // frenado
  rect(3*width/4-4*sqw, 3*width/4-sqw, 6*sqw, 3*sqw); 
  // velocidad
  rect(width/2-2.75*sqw, width/4+sqw, 5.5*sqw, 3*sqw);
  fill (225);  
  text("Aceleración [m/s^2]:", width/4-sqw, 3*width/4);
  text(a, width/4+sqw/2, 3*width/4+sqw);
  text("Frenado [m/s^2]:", 3*width/4-2.5*sqw, 3*width/4);
  text(f, 3*width/4-3*sqw/2, 3*width/4+sqw);
  // velocidad
  text("Velocidad [m/s]:", width/2-1.5*sqw, width/4+2*sqw);
  text(v, width/2-0.75*sqw, width/4+3*sqw);
}
void sliders(){
  noFill ();
  stroke(225);
  //VELOCIDAD
  //barra numerada
  line(width/4-2*sqw, width/4-sqw/2, width - width/4+2*sqw, width/4-sqw/2);
  for (int i=0; i<=20; i++){
    line( width/4-2*sqw+ind_array.get(i)*22.5,width/4-2*sqw, width/4-2*sqw+ind_array.get(i)*22.5, width/4-sqw/2);
  };
  for (int i = 0; i<=10; i++){
    fill (225);
    text(i*200,width/4-2.25*sqw+i*22.25*2,width/4);
  }
  //barra slider
  noFill ();
  line(width/4-2*sqw+v*450/2000,width/4-3.5*sqw, width/4-2*sqw+v*450/2000, width/4-2.5*sqw);
  //ACELERACION
  //barra numerada
  line(width/4-2*sqw, 3*width/4-2*sqw, width/4-2*sqw,  3*width/4-2*sqw-20*ind_h);
  for (int i=0; i<=20; i++){
    line(width/4-2*sqw,3*width/4-2*sqw-ind_array.get(i)*ind_h, width/4-sqw, 3*width/4-2*sqw-ind_array.get(i)*ind_h);
  };
  for (int i = 0; i<=10; i++){
    fill (225);
    text(i*2,width/4-2.75*sqw,3*width/4-2*sqw-ind_array.get(i)*2*ind_h+ind_h/2);
  };
  //barra slider
  noFill ();
  line(width/4,3*width/4-2*sqw-ind_h*a, width/4+sqw, 3*width/4-2*sqw-ind_h*a);
  //FRENADO
  //barra numerada
  line(3*width/4+2*sqw, 3*width/4-2*sqw, 3*width/4+2*sqw,  3*width/4-2*sqw-20*ind_h);
  for (int i=0; i<=20; i++){
    line(3*width/4+sqw,3*width/4-2*sqw-ind_array.get(i)*ind_h, 3*width/4+2*sqw, 3*width/4-2*sqw-ind_array.get(i)*ind_h);
  };
  for (int i = 0; i<=10; i++){
    fill (225);
    text(i*2,3*width/4+2.25*sqw,3*width/4-2*sqw-ind_array.get(i)*2*ind_h+ind_h/2);
  }
  //barra slider
  noFill ();
  line(3*width/4-sqw,3*width/4-2*sqw-ind_h*f, 3*width/4, 3*width/4-2*sqw-ind_h*f);
}
